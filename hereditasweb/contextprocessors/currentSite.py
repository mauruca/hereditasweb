from django.conf import settings

def site_proccess(request):
    baseapipath =  settings.E_HER_API_URL + "/apiX/"
    baseapipathmetricas = baseapipath + "metricasX/"
    return {
        'apimetricascontagem': baseapipathmetricas + 'contagem',
        'apimetricasEstadoBem': baseapipathmetricas + 'estadoBem',
        'apimetricasSituacaoBem': baseapipathmetricas + 'situacaoBem',
        'apimapa': baseapipath + 'mapaX/',
        'apibem': baseapipath + 'bemX/',
        'apibemagrupado': baseapipath + 'bemAgrupX/',
        'apidepreciacao': baseapipath + 'depreciacaoX/',
        'apiinventario': baseapipath + 'inventarioX/',
        'apimovimento': baseapipath + 'movimentoX/',
        'apiauth': baseapipath + 'api-token-auth/'
     }
