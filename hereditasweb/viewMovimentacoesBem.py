# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewMovimentacoesBem(TemplateView):
    template_name = "listMovimentacoes.html"

    def get_context_data(self, **kwargs):
        context = super(ViewMovimentacoesBem, self).get_context_data(**kwargs)
        context['listtitle'] = 'Movimentações'
        return context
