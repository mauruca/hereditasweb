# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewInventario(TemplateView):
    template_name = "listInventario.html"

    def get_context_data(self, **kwargs):
        context = super(ViewInventario, self).get_context_data(**kwargs)
        context['listtitle'] = 'Inventários'
        return context
