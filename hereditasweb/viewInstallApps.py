# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewInstallWindows(TemplateView):
    template_name = "installWindows.html"

class ViewInstallAndroid(TemplateView):
    template_name = "installAndroid.html"
