# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

"""hereditas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

# hereditas
from .viewIndex import ViewIndex
from .viewAuth import ViewAuth
from .viewInventario import ViewInventario
from .viewDepreciacao import ViewDepreciacao
from .viewBem import ViewBem
from .viewBemAgrupado import ViewBemAgrupado
from .viewInstallApps import ViewInstallWindows, ViewInstallAndroid
from .viewMovimentacoesBem import ViewMovimentacoesBem

# patterns
urlpatterns = [
    url(r'^$',ViewIndex.as_view(), name="index"),
    url(r'^login', ViewAuth.signin, name="login"),
    url(r'^logout', ViewAuth.signout, name="logout"),
    url(r'^bem/$', ViewBem.as_view(), name="bem"),
    url(r'^bem/agrupado/$', ViewBemAgrupado.as_view(), name="bem/agrupado"),
    url(r'^depreciacao/$', ViewDepreciacao.as_view(), name="depreciacao"),
    url(r'^inventario/$', ViewInventario.as_view(), name="inventario"),
    url(r'^movimentacoes/$', ViewMovimentacoesBem.as_view(), name="movimentacoes"),
    url(r'^instalaAndroid/$', ViewInstallAndroid.as_view(), name="instalaAndroid"),
    url(r'^instalaWindows/$', ViewInstallWindows.as_view(), name="instalaWindows"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
