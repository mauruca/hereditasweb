# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewBemAgrupado(TemplateView):
    template_name = "listBensAgrupados.html"

    def get_context_data(self, **kwargs):
        context = super(ViewBemAgrupado, self).get_context_data(**kwargs)
        context['listtitle'] = 'Bens Agrupados'
        return context
