# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewIndex(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(ViewIndex, self).get_context_data(**kwargs)
        return context
