# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .formAuth import SigninForm
from rest_framework.authtoken.models import Token

class ViewAuth:
    def signin(request):
        msgerro = None
        if request.method != "POST":
            msgerro = "Solicitação inválida."
            response_data = {"msgerro": msgerro,}
            return render(request,"index.html",response_data)

        form = SigninForm(request.POST)

        if not form.is_valid():
            msgerro = "Formulário inválido."
            response_data = {"msgerro": msgerro,'form': form}
            return render(request,"index.html",response_data)

        pusernameemail = form.cleaned_data['inputUsernameEmail']
        psenha = form.cleaned_data['inputPassword']
        auxuser = None
        try:
            if pusernameemail and User.objects.filter(email=pusernameemail).count() > 1:
                msgerro = "Cadastro inválido. e-Mail duplicado."
                response_data = {"msgerro": msgerro, }
                return render(request,"index.html",response_data)
            if pusernameemail and User.objects.filter(email=pusernameemail).count() == 1:
                auxuser = User.objects.get(email=pusernameemail).username
        except User.DoesNotExist:
            msgerro = "Usuário ou email, e senha incorretos."
            response_data = {"msgerro": msgerro, }
            return render(request,"index.html",response_data)
        if auxuser:
            username = auxuser
        else:
            username = pusernameemail

        user = authenticate(username=username, password=psenha)
        if user:
            # the password verified for the user
            if user.is_active:
                login(request,user)
                try:
                    token = Token.objects.get(user=user)
                    response = redirect(reverse('index'))
                    if token:
                        token = token.key
                        response.set_cookie(key='token', value=token)
                    return response
                    #return redirect(reverse('index'))
                except Token.DoesNotExist:
                    logout(request)
                    msgerro = "Sem permissão para acesso a API. Solicitar token."
            else:
                msgerro = "Usuário está desativado."
        else:
            # the authentication system was unable to verify the username and password
            msgerro = "Usuário ou email, e senha incorretos."

        return render(request,"index.html",{"msgerro": msgerro,})

    def signout(request):
        logout(request)
        msgsucesso = "Usuário foi desconectado."
        response_data = {"msgsucesso": msgsucesso, }
        return render(request,"index.html",response_data)
