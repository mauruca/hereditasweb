
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django import forms

class SigninForm(forms.Form):
    inputUsernameEmail = forms.CharField(required=True)
    inputPassword = forms.CharField(required=True)
