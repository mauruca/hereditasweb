# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewDepreciacao(TemplateView):
    template_name = "listDepreciacao.html"

    def get_context_data(self, **kwargs):
        context = super(ViewDepreciacao, self).get_context_data(**kwargs)
        context['listtitle'] = 'Depreciações'
        return context
