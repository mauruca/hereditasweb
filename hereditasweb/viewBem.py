# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.views.generic.base import TemplateView

class ViewBem(TemplateView):
    template_name = "listBens.html"

    def get_context_data(self, **kwargs):
        context = super(ViewBem, self).get_context_data(**kwargs)
        context['listtitle'] = 'Bens'
        return context
