#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

ver="$(cat version)"

docker build $1 -t hereditas/web:$ver .
docker build -t hereditas/web .
docker images hereditas/web
