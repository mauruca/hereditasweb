var mensagemErro = function(jqXHR, textStatus, errorThrown,titulo ) {
    $('#hereditasListTable_processing').hide();
    mensagemErroAjaxBase(jqXHR, textStatus, errorThrown, titulo);
}

var mensagemSucesso = function(mensagem,titulo) {
    $('#hereditasListTable_processing').hide();
    mensagemSucessoBase(mensagem,titulo);
}

var mensagemAlerta = function(mensagem,titulo) {
    $('#hereditasListTable_processing').hide();
    mensagemAlertaBase(mensagem,titulo);
}

var mensagemInfo = function(mensagem,titulo) {
    $('#hereditasListTable_processing').hide();
    mensagemInfoBase(mensagem,titulo);
}
