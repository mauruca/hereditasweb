var carregaContagensMetricas = function(data) {
  $.each(data, function(key, val) {
    $('#' + key).html(val);
  });
}

var metricasContagem = function(apimetricascontagem) {
  $.getJSON( apimetricascontagem , function(data) {
    carregaContagensMetricas(data);
    drawChartbensPorEtiquetagem(data);
    drawChartbensPorEtiquetagemValor(data);
  })
  .fail(function( jqxhr, textStatus, error ) {
    alert('Erro ao buscar os dados de contagem das métricas.');
    mensagemErroBase(jqxhr, textStatus, error);
  });
}

var metricasEstadoBem = function(url) {
  $.getJSON( url, function(data) {
    var dados = JSON.parse("[" + data.bensporEstadoDados.replace(/'/g, '"') + "]");
    drawChartBensPorEstado(dados);
  })
  .fail(function( jqxhr, textStatus, error ) {
    alert('Erro ao buscar os dados de estado dos bens.');
    mensagemErroBase(jqxhr, textStatus, error);
  });
}

var metricasSituacaoBem = function(url) {
  $.getJSON( url, function(data) {
    var dados = JSON.parse("[" + data.bensporSituacaoDados.replace(/'/g, '"') + "]");
    drawChartBensPorSituacao(dados);
  })
  .fail(function( jqxhr, textStatus, error ) {
    alert('Erro ao buscar os dados de situação dos bens.');
    mensagemErroBase(jqxhr, textStatus, error);
  });
}
