var getMapa = function(apimapa) {
    $.ajax({
      type: "POST",
      url: apimapa,
      error: function(jqXHR,status,errordata){
          trataErro();
      },
      success: function(data){
          buildMap(data);
      }
  });
}

var w = $("#mapa").width(),
    h = window.innerHeight,
    x = d3.scale.linear().range([0, w]),
    y = d3.scale.linear().range([0, h]),
    color = d3.scale.category20c(),
    root,
    node;

var treemap = d3.layout.treemap()
    .round(false)
    .size([w, h])
    .sticky(true)
    .value(function(d) { return d.size; });

var svg = d3.select("#mpbResultado").append("div")
    .attr("class", "chart")
    .style("width", w + "px")
    .style("height", h + "px")
    .append("svg:svg")
    .attr("width", '85%')
    .attr("height", '100%')
    .attr('viewBox','0 0 '+h+' '+w)
    .attr('preserveAspectRatio','xMinYMin')
    .append("svg:g")
    .attr("transform", "translate(.5,.5)");

var svgl = d3.select("#mpbLegenda").append("div")
    .attr("class", "chart")
    .style("width", 200 + "px")
    .style("height", 150 + "px")
    .append("svg:svg")
    .attr("width", 200)
    .attr("height", 150)
    .append("svg:g")
    .attr("transform", "translate(.5,.5)");

var legendRectSize = 10;
var legendSpacing = 4;

var mapaposicao = function(jsondata_url) {
  d3.json(jsondata_url, buildMap);
}

buildMap = function(data) {

  node = root = data;

  var nodes = treemap.nodes(root)
      .filter(function(d) { return !d.children; });

  var parents = treemap.nodes(root)
      .filter(function(d) { return d.children && d.name != 'base'; });

  if(parents.length == 0)
  {
    trataSemDados();
    return;
  }

  var cell = svg.selectAll("g")
      .data(nodes)
    .enter().append("svg:g")
      .attr("class", "cell")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
      .on("click", function(d) { return zoom(node == d.parent ? root : d.parent); });

  cell.append("svg:rect")
      .attr("width", function(d) { return d.dx - 1; })
      .attr("height", function(d) { return d.dy - 1; })
      .style("fill", function(d) { return color(d.parent.name); })

  cell.append("svg:text")
      .attr("x", function(d) { return d.dx / 2; })
      .attr("y", function(d) { return d.dy / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .text(function(d) { return d.name; })
      .style("opacity", function(d) { d.w = this.getComputedTextLength(); return d.dx > d.w ? 1 : 0; });

  d3.select(window).on("click", function() { zoom(root); });

  d3.select("select").on("change", function() {
    treemap.value(this.value == "size" ? size : count).nodes(root);
    zoom(node);
  });

  var legend = svgl.selectAll('.legend')
    .data(parents)
    .enter()
    .append('g')
    .attr('class', 'legend')
    .attr('transform', function(d, i) {
      var height = legendRectSize + legendSpacing;
      var offset =  height * color.domain().length / 2;
      var horz = 0;
      var vert = i * height + 10;
      return 'translate(' + horz + ',' + vert + ')';
    });

  legend.append('rect')
    .attr('width', legendRectSize)
    .attr('height', legendRectSize)
    .style('fill', function(d) { return color(d.name); })
    .style('stroke', function(d) { return color(d.name); });

  legend.append('text')
    .attr('x', legendRectSize + legendSpacing)
    .attr('y', legendRectSize)
    .text(function(d) { return d.name; });

  $("#mpbMensagem").alert('close');
}

function size(d) {
  return d.size;
}

function count(d) {
  return 1;
}

function zoom(d) {
  var kx = w / d.dx, ky = h / d.dy;
  x.domain([d.x, d.x + d.dx]);
  y.domain([d.y, d.y + d.dy]);

  var t = svg.selectAll("g.cell").transition()
      .duration(d3.event.altKey ? 7500 : 750)
      .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

  t.select("rect")
      .attr("width", function(d) { return kx * d.dx - 1; })
      .attr("height", function(d) { return ky * d.dy - 1; })

  t.select("text")
      .attr("x", function(d) { return kx * d.dx / 2; })
      .attr("y", function(d) { return ky * d.dy / 2; })
      .style("opacity", function(d) { return kx * d.dx > d.w ? 1 : 0; });

  node = d;
  d3.event.stopPropagation();
}

var trataErro = function() {
    $("#mpbMensagem").toggleClass('alert-danger','alert-info');
    $("#mpbMensagem").html('Erro ao buscar dados. Clique no menu Indicadores para tentar novamente.');
}

var trataSemDados = function() {
    $("#mpbMensagem").html('Nenhum inventário foi encontrado.');
}
