var drawChartBensPorEstado = function (dados) {
  var data = google.visualization.arrayToDataTable(dados);
  var options = {
    title: 'Bens por estado',
    pieHole: 0.4,
    pieStartAngle: 180,
    colors:['#38acff','#003061','#1ebda8','#2d5d8c','#4de8d3','#66dbff'],
    chartArea:{left:0,top:30,width:'360',height:'260'},
    fontSize: 16, height: 300, width: 400,
    legend: {position: 'right', textStyle: {fontSize: 12}}
  };
  var chart = new google.visualization.PieChart(document.getElementById('bensPorEstado'));
  chart.draw(data, options);
}

var drawChartBensPorSituacao = function (dados) {
  var data = google.visualization.arrayToDataTable(dados);
  var options = {
    title: 'Bens por situação',
    pieHole: 0.4,
    pieStartAngle: 180,
    colors:['#38acff','#003061','#1ebda8','#2d5d8c','#4de8d3','#66dbff'],
    chartArea:{left:0,top:30,width:'360',height:'260'},
    fontSize: 16, height: 300, width: 400,
    legend: {position: 'right', textStyle: {fontSize: 12}}
  };
  var chart = new google.visualization.PieChart(document.getElementById('bensPorSituacao'));
  chart.draw(data, options);
}

var drawChartbensPorEtiquetagem = function (dados) {
  var data = google.visualization.arrayToDataTable([
    ['Classificacao', 'Qtd bens'],
    ['Etiquetados',dados.contagemBensEtiquetados],
    ['Não etiquetados',dados.contagemBensNaoEtiquetados]
  ]);
  var options = {
    title: 'Bens com etiqueta',
    pieHole: 0.4,
    pieStartAngle: 180,
    colors:['#38acff','#003061','#1ebda8','#2d5d8c','#4de8d3','#66dbff'],
    chartArea:{left:0,top:30,width:'360',height:'260'},
    fontSize: 16, height: 300, width: 400,
    legend: {position: 'right', textStyle: {fontSize: 12}}
  };
  var chart = new google.visualization.PieChart(document.getElementById('bensPorEtiquetagem'));
  chart.draw(data, options);
}

var drawChartbensPorEtiquetagemValor = function (dados) {
  var data = google.visualization.arrayToDataTable([
    ['Classificacao', 'Valor bens'],
    ['Etiquetados',dados.valorBensEtiquetados],
    ['Não etiquetados',dados.valorBensNaoEtiquetados]
  ]);
  var options = {
    title: 'R$ bens com etiqueta',
    pieHole: 0.4,
    pieStartAngle: 180,
    colors:['#38acff','#003061','#1ebda8','#2d5d8c','#4de8d3','#66dbff'],
    chartArea:{left:0,top:30,width:'360',height:'260'},
    fontSize: 16, height: 300, width: 400,
    legend: {position: 'right', textStyle: {fontSize: 12}}
  };
  var chart = new google.visualization.PieChart(document.getElementById('valorPorEtiquetagem'));
  chart.draw(data, options);
}

var drawChartbensPorQuantidade = function (dados) {

  var data = google.visualization.arrayToDataTable(dados);

  var options = {
    title: 'Bens por quantidade',
    legend: { position: 'none' },
    axes: {
      x: {
        0: { side: 'top', label: 'Qtd'} // Top x-axis.
      }
    },
    chartArea: {height: '80%'},

  };

  var chart = new google.visualization.BarChart(document.getElementById('bensPorQuantidade'));

  chart.draw(data, options);
}
