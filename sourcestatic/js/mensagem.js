var mensagemErroBase = function(mensagem, titulo) {
    $("#alertMensagem").attr('class', 'alert-danger');
    $("#alertMensagem").addClass('alert');
    if(titulo != undefined)
    {
      $("#alertTitulo").html(titulo);
    }
    $("#alertMensagem").html('<strong>Ah Não! Que vergonha!</strong> ' + mensagem);
    $('#mensagemModal').modal('show');
}

var mensagemErroAjaxBase = function(jqXHR, textStatus, errorThrown, titulo) {
    var errAjax = "";
    if(jqXHR != undefined)
    {
        if(jqXHR.responseText != undefined)
        {
            errAjax = $.parseJSON(jqXHR.responseText)['detail'];
        }
        if(errAjax.length == 0)
        {
            errAjax = "Erro ao buscar dados no servidor.";
        }
    }
    mensagemErroBase(errAjax,titulo);
}

var mensagemSucessoBase = function(mensagem,titulo) {
    $("#alertMensagem").attr('class', 'alert-success');
    $("#alertMensagem").addClass('alert');
    if(titulo != undefined)
    {
      $("#alertTitulo").html(titulo);
    }
    $("#alertMensagem").html('<strong>Uhu!</strong> ' + mensagem);
    $('#mensagemModal').modal('show');
}

var mensagemAlertaBase = function(mensagem,titulo) {
    $("#alertMensagem").attr('class', 'alert-warning');
    $("#alertMensagem").addClass('alert');    
    if(titulo != undefined)
    {
      $("#alertTitulo").html(titulo);
    }
    $("#alertMensagem").html('<strong>Pera!! Se liga!</strong> ' + mensagem);
    $('#mensagemModal').modal('show');
}

var mensagemInfoBase = function(mensagem,titulo) {
    if(titulo != undefined)
    {
      $("#alertTitulo").html(titulo);
    }
    $("#alertMensagem").html('<strong>Oi! Me leia!!</strong> ' + mensagem);
    $('#mensagemModal').modal('show');
}
