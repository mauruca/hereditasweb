# [Hereditas Web Dashboard](http://www.hereditas.net.br/)  

This project contains the dashboard site for Hereditas inventory control platform.  

The system is based on [Django](https://www.djangoproject.com/).  

## The base dependencies
To make sure you have all base dependencies installed:
* python3
* pip  

If you are running a debian linux or any other distribution based on apt, you can use:
```bash
$ shellscript/installbase.sh
```  

## Setting up for local build
Create a virtual environment to work with instead of installing packages to all users running the command 
```bash
$ ./_install_packages.sh
$ ./_setup_environment.sh
```

The django settings file is expecting some enviroment variables to execute, but we have some helper scripts exporting them:  
HER_USE_LITE="true" - Use SqlLite database  
HER_DB_NAME="database"  - Use SqlLite database  
HER_DB_USER="databseadmin" - database name  
HER_DB_PW="databaseadminpassword" - database pw  
HER_DB_HOST="dbserver" - database server  
HER_PROD="false" - is production  
HER_DEBUG="true" - debug mode  
HER_HOSTSALLOWED="" - hosts allowed to request  
HER_GOOGLE_ANALYTICS="" - google analytics ID  
HER_SECPX="false" - using ssl connection with proxy

## Starting up
You can develop to this project in using several ways:

### As a normal Django project using python commands:
Execute the setup.sh before executing the commands below:  

* activate virtual environment
```bash
$ source env/bin/activate
```
* make migrations
```bash
$ python3 manage.py makemigrations
```
* execute migrations
```bash
$ python3 manage.py migrate
```
* run the server specifing the port
```bash
$ python3 manage.py runserver 9000
```
The command above start the development server at http://localhost:9000.
* run the automated tests
```bash
$ python3 manage.py test
```

### As a normal Django project using a helper script:
* The script runs makimigrations, migrate and runserver command:
```bash
$ ./3.runserver.sh 8000 http://localhost:9000
```
The command above start the development server at http://localhost:8000.

* To run the automated tests, just write tests instead of port number.
```bash
$ ./3.runserver.sh test http://localhost:9000
```

### As a normal Django project using a helper script and a postgres database server.
You don't need to install postgres on your development computer. It will use a Docker container to take care of it.
First install Docker and then run the command adding a second argument:
```bash
$ ./3.runserver.sh 8000 http://localhost:9000 postgres
```
To run the tests using the postgres database server:
```bash
$ ./3.runserver.sh test http://localhost:9000 postgres
```
### As a composition of docker containers simulating production architecture:
Just run the command below and it will start 5 containers: 2 proxies, hereditas web dashboard, hereditas io API and postgres.
```bash
$ ./4.composerun.sh 9000 8000
```
Open a browser at http://localhost:9000. The pre-defined administrator is hadmin and the password is mudar123.
To stop the compose type ctrl+c.

## Build

### Manual build
If you are using the manual Django commands use the following script to build:
```bash
$ ./_static_build.sh
```

### Container build
The first build the static files and the second build the image. TODO use a multi-build dockerfile.
```bash
$ ./0.dockerbuild.sh
$ ./1.build.sh
```


## Bugs and Issues
Have a bug or an issue with this theme? [Open a new issue](https://github.com/ultimaratioregis/hereditasadmin/issues) here on GitHub.

## Copyright and License
Copyright (C) 2015-2017 Mauricio Costa Pinheiro. All rights reserved. Read the LICENSE file for details.
