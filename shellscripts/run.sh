#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.
#
# Você deve ter recebido uma cópia da licença que está no arquivo LICENCA ou LICENSE. Se não,
# entrar em contato escrevendo para mpinheiro@pobox.com ou mauricio.pinheiro@gmail.com ou mauricio@ur2.com.br.
echo "Starting Hereditas Web..."

if [ -z "$1" ]; then
    echo "./run.sh <waitsec>"
    exit 1
fi

echo "wating $1s"
sleep $1

echo " "
echo "Running migrations..."
python3 manage.py makemigrations --no-input
python3 manage.py migrate --no-input

echo " "
echo "Running server..."
gunicorn hereditasweb.wsgi:application -t 120 -w 1 -b 0.0.0.0:8000 --log-level=error