#!/bin/bash

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
hpgdata=$(docker ps --all | grep hpgdata)
hereditasdb=$(docker ps | grep hereditasdb)
removeCT=$(docker ps --all | grep hereditasdb)

if [ "${#hpgdata}" -eq 0 ]
then
  echo "creating database volume..."
  docker create -v /var/lib/postgresql/data/pgdata --name hpgdata postgres:alpine /bin/true
  sleep 2
fi

if [ "${#hereditasdb}" -eq 0 ]
then
  if [ "${#removeCT}" -ne 0 ]
  then
    docker rm hereditasdb
  fi

  if [ -z "$1" ]
    then
      echo "Informar o nome do usuário da base."
      exit 1
  fi

  if [ -z "$2" ]
    then
      echo "Informar senha do usuário da base."
      exit 1
  fi

  echo "starting postgres database..."
  docker run -d --name hereditasdb -e PGDATA=/var/lib/postgresql/data/pgdata -e POSTGRES_DB=postgres -e POSTGRES_USER=$1 -e POSTGRES_PASSWORD=$2 --volumes-from hpgdata -p 5432:5432 postgres:alpine
fi
