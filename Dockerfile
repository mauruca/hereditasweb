# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

FROM hereditas/baseserver:4.1

LABEL author="Mauricio Costa Pinheiro"
LABEL maintainer="mauricio@hereditas.net.br"

# Install requirements
COPY requirements requirements
RUN pip3 install --upgrade -r requirements

EXPOSE 8000

VOLUME ["/usr/src/app/static"]

# Copy static files that will be served by nginx proxy
COPY static /usr/src/app/static

# Copy app files
COPY ./shellscripts/run.sh /usr/src/app/
COPY ./*.py /usr/src/app/
COPY ./hereditasweb /usr/src/app/hereditasweb
COPY ./LICENSE* /usr/src/app/

# Generates secret key
RUN mkdir /etc/hereditas && openssl rand -base64 50 > /etc/hereditas/sk

CMD ./run.sh
