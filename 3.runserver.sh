#!/bin/bash

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

webport="$1"
apiport="$2"
postgres="$3"

if [ -z "$port" ]
  then
    echo "./rundevserver <webport> <apiport> <optional postgres>"
    exit 1
fi

if [ -z "$apiport" ]
  then
    echo "./rundevserver <webport> <apiport> <optional postgres>"
    exit 1
fi

echo " "
echo "starting dev environment..."
./_setup_virtualenv.sh
./_setup_environment.sh
./_install_packages.sh
source env/bin/activate

echo " "
echo "creating secret file..."
openssl rand -base64 50 > sk

# sec issues
export HER_DB_USER="herad"
export HER_DB_PW="herad"
export HER_SECPX="false"
export HER_ADMIN="had"
export HER_ADMIN_PW="mudar1234"
export HER_PROD="false"
export HER_DEBUG="true"

# db issues - exporta para rodar os comandos na config dbadmshell e criar o banco de dados
export HER_USE_LITE="true"
export HER_DB_NAME="hereditas"
export HER_DB_HOST="hdb"
dbserverip=$(docker inspect $HER_DB_HOST | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['NetworkSettings']['Networks']['hereditasio_hereditasnet']['IPAddress'])")
export HER_DB_HOST="$dbserverip"

# hosts
export HER_WEBPORT="$webport"
export HER_APIPORT="$apiport"
export HER_HOSTSALLOWED="localhost"
export HER_API_URL="http://$HER_HOSTSALLOWED:$HER_APIPORT"

# analytics
export HER_GOOGLE_ANALYTICS=""
export HER_ADMIN_EMAIL="admin@hereditas.net.br"

if [ "$postgres" == "postgres" ]
then
  export HER_USE_LITE="false"

  hpgdata=$(docker ps --all | grep hpgdata)
  echo " "
  echo "starting container database..."
  shellscripts/dockerpg.sh $HER_DB_USER $HER_DB_PW
  sleep 5

  if [ "${#hpgdata}" -eq 0 ]
  then
    echo "creating database hereditas..."
    echo "CREATE DATABASE hereditas WITH ENCODING='UTF8' OWNER=$HER_DB_USER CONNECTION LIMIT=-1;" | python3 manage.py dbshell --database=dbadmshell
  fi
fi

echo " "
echo "Build migrations..."
python3 manage.py makemigrations --noinput
python3 manage.py migrate

if [ "$1" == "test" ]
then
  echo " "
  echo "starting tests..."
  python3 manage.py test
else
  echo " "
  echo "starting server..."
  python3 manage.py runserver 127.0.0.1:$HER_WEBPORT
fi
