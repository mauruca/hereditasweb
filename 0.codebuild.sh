#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
bver="$(cat baseserver)"
echo " "
echo "Starting the build container..."
echo " "
#docker run --rm -it --cpus=".10" -u $(id -u):$(id -g) -e HOME=/build/env -e PATH=$PATH:/build/env/.local/bin -v $(pwd):/build hereditas/baseserver:$bver /bin/bash -c "cd /build;./_setup_virtualenv.sh;./_setup_environment.sh;./_install_packages.sh;./_static_build.sh"

docker run --rm -it -u $(id -u):$(id -g) -e HOME=/build/env -e PATH=$PATH:/build/env/.local/bin -v $(pwd):/build hereditas/baseserver:$bver /bin/bash -c "cd /build;./_setup_virtualenv.sh;./_setup_environment.sh;./_install_packages.sh;./_static_build.sh"

#docker run --rm -it -v $(pwd):/build hereditas/baseserver /bin/bash -c "cd /build;./_setup_environment.sh;/bin/bash"