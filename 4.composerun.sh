#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

webport="$1"
apiport="$2"

if [ -z "$webport" ]; then
    echo "./runcompose.sh <webport> <apiport>"
    exit 1
fi

if [ -z "$apiport" ]; then
    echo "./runcompose.sh <webport> <apiport>"
    exit 1
fi

# sec issues
export HER_DB_USER="herad"
export HER_DB_PW="herad"
export HER_SECPX="false"
export HER_ADMIN="had"
export HER_ADMIN_PW="mudar1234"
export HER_PROD="false"
export HER_DEBUG="true"

# db issues - exporta para rodar os comandos na config dbadmshell e criar o banco de dados
export HER_USE_LITE="false"
export HER_DB_NAME="hereditas"
export HER_DB_HOST="hdb"

# hosts
export HER_WEBPORT="$webport"
export HER_APIPORT="$apiport"
export HER_HOSTSALLOWED="localhost"
export HER_API_URL="http://$HER_HOSTSALLOWED:$HER_APIPORT"

# io
export HER_WEBPORT="$HER_WEBPORT"

# analytics
export HER_GOOGLE_ANALYTICS=""
export HER_ADMIN_EMAIL="admin@hereditas.net.br"

# pgadmin
export PGADMIN_DEFAULT_PASSWORD="teste"


echo " "
echo "Starting Hereditas Web container @ http://$HER_HOSTSALLOWED:$HER_WEBPORT"
docker-compose -f compose.yml stop
docker-compose -f compose.yml up --force-recreate