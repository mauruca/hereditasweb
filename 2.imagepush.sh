#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

tagversion="$(cat version)"
docker push hereditas/web:$tagversion
docker push hereditas/web:latest